import com.ibm.icu.text.Transliterator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class TestData {

    Random random = new Random();

    ArrayList<String> attributes = readTextFile("/Attributes");
    ArrayList<String> femaleFirstName = readTextFile("/FemaleFirstName");
    ArrayList<String> femaleLastName = readTextFile("/FemaleLastName");
    ArrayList<String> femalePatronymic = readTextFile("/FemalePatronymic");
    ArrayList<String> maleFirstName = readTextFile("/MaleFirstName");
    ArrayList<String> maleLastName = readTextFile("/MaleLastName");
    ArrayList<String> malePatronymic = readTextFile("/MalePatronymic");
    ArrayList<String> countries = readTextFile("/Countries");
    ArrayList<String> cities = readTextFile("/Cities");
    ArrayList<String> street = readTextFile("/Street");



    public String gender[] = {"male","female"};

    public static ArrayList readTextFile(String txtFilePath) {
        ArrayList<String> dataModels = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    TestData.class.getClass().getResourceAsStream(txtFilePath)
            ));
            String strLine;
            while ((strLine = br.readLine()) != null){
                dataModels.add(translit(strLine));
            }
        }
        catch (Exception e) {
        }
        return dataModels;
    }

    public static void printList(ArrayList<String> dataModels){
        for (int i = 0; i < dataModels.size(); i++)
            System.out.println(dataModels.get(i));
    }

    public String getFemaleFirstName() {
        return femaleFirstName.get(random.nextInt(30));
    }


    public String getFemaleLastName() {
        return femaleLastName.get(random.nextInt(30));
    }

    public String getFemalePatronymic() {
        return femalePatronymic.get(random.nextInt(30));
    }

    public String getMaleFirstName() {
        return maleFirstName.get(random.nextInt(30));
    }

    public String getMaleLastName() {
        return maleLastName.get(random.nextInt(30));
    }

    public String getMalePatronymic() {
        return malePatronymic.get(random.nextInt(30));
    }

    public String getGender() {
        return gender[Tools.getRandomInRange(0,1)];
    }

    public String getCountry() {
        return countries.get(random.nextInt(22));
    }

    public String getCity() {
        return cities.get(random.nextInt(30));
    }

    public String getStreet() {
        return street.get(random.nextInt(30));
    }

    public static final String CYRILLIC_TO_LATIN = "Cyrillic-Latin";

    public static String translit(String st){
        Transliterator toLatinTrans = Transliterator.getInstance(CYRILLIC_TO_LATIN);
        String result = toLatinTrans.transliterate(st);
        return  result;
    }

}

