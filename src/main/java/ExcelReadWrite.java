import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ExcelReadWrite {

    TestData testData = new TestData();

    public HSSFWorkbook readWorkbook(String filename) {
        try {
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filename));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            return wb;
        }
        catch (Exception e) {
            return null;
        }
    }

    public void writeWorkbook(HSSFWorkbook wb, String fileName) {
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            File file = new File(fileName);
            wb.write(fileOut);
            fileOut.close();
            System.out.println("File path: "+ file.getAbsolutePath());

        }
        catch (Exception e) {
        }
    }

    public void createWorkBook(String filePath){
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("people");
        HSSFRow row = sheet.createRow(0);
        for (int i = 0; i < testData.attributes.size(); i++){
            row.createCell(i).setCellValue(testData.attributes.get(i));
        }
        writeWorkbook(workbook, filePath);
    }

    public void fillRow(String filePath){
        HSSFWorkbook workbook = readWorkbook(filePath);
        HSSFSheet sheet = workbook.getSheet("people");
        HSSFRow row;
        PersonModel pm;
        for (int i = 0; i < 30; i++) {
            pm = new Handbook().getPerson();
            row = sheet.createRow(i+1);
            row.createCell(0).setCellValue(pm.getLastName());
            row.createCell(1).setCellValue(pm.getName());
            row.createCell(2).setCellValue(pm.getPatronymic());
            row.createCell(3).setCellValue(pm.getAge());
            row.createCell(4).setCellValue(pm.getGender());
            row.createCell(5).setCellValue(pm.getDateOfBirth().toString());
            row.createCell(6).setCellValue(pm.getInn());
            row.createCell(7).setCellValue(pm.getPostIndex());
            row.createCell(8).setCellValue(pm.getCity());
            row.createCell(9).setCellValue(pm.getStreet());
            row.createCell(10).setCellValue(pm.getHouseNumber());
            row.createCell(11).setCellValue(pm.getFlatNumber());
        }
        writeWorkbook(workbook, filePath);

    }
}

