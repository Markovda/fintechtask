import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;

public class Pdfchik {
    Document document = new Document();
    TestData testData = new TestData();

    public void pdftest() {

        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("AddTableExample.pdf"));
            document.open();

            PdfPTable table = new PdfPTable(testData.attributes.size()); // 3 columns.
            table.setWidthPercentage(100); //Width 100%
            table.setSpacingBefore(10f); //Space before table
            table.setSpacingAfter(10f); //Space after table

            //Set Column widths
           // float[] columnWidths = {1f, 1f, 1f};
            //table.setWidths(columnWidths);
            PersonModel pm;
            String string = new String();
            for (int i = 0; i < testData.attributes.size(); i++){
                table.addCell(new PdfPCell(new Paragraph(testData.attributes.get(i))));
            }
            for (int i = 0; i < 30; i++){
                pm = new Handbook().getPerson();
                table.addCell(new PdfPCell(new Paragraph(pm.getLastName())));
                table.addCell(new PdfPCell(new Paragraph(pm.getName())));
                table.addCell(new PdfPCell(new Paragraph(pm.getPatronymic())));
                table.addCell(new PdfPCell(new Paragraph(String.valueOf(pm.getAge()))));
                table.addCell(new PdfPCell(new Paragraph(pm.getGender())));
                table.addCell(new PdfPCell(new Paragraph(pm.getDateOfBirth().toString())));
                table.addCell(new PdfPCell(new Paragraph(String.valueOf(pm.getInn()))));
                table.addCell(new PdfPCell(new Paragraph(String.valueOf(pm.getPostIndex()))));
                table.addCell(new PdfPCell(new Paragraph(pm.getCity())));
                table.addCell(new PdfPCell(new Paragraph(pm.getStreet())));
                table.addCell(new PdfPCell(new Paragraph(pm.getHouseNumber())));
                addStringToCell(table, String.valueOf(pm.getFlatNumber()));
            }
            document.add(table);
            document.close();
            writer.close();

        } catch (Exception e) {

        }
    }

    public void addStringToCell(PdfPTable table, String data){
        table.addCell(new PdfPCell(new Paragraph(data)));

    }

}
