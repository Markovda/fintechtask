import java.time.LocalDate;

public class Handbook {
    PersonModel personModel = new PersonModel();
    TestData testData = new TestData();

    Handbook(){

        personModel.setGender(testData.getGender());
        if(personModel.getGender().equals("female")) {
            personModel.setName(testData.getFemaleFirstName());
            personModel.setLastName(testData.getFemaleLastName());
            personModel.setPatronymic(testData.getFemalePatronymic());
        } else if(personModel.getGender().equals("male")) {
            personModel.setName(testData.getMaleFirstName());
            personModel.setLastName(testData.getMaleLastName());
            personModel.setPatronymic(testData.getMalePatronymic());
        }
        personModel.setDateOfBirth(LocalDate.of(Tools.getRandomInRange(1950, 2000), Tools.getRandomInRange(1, 12), Tools.getRandomInRange(1, 27)));
        personModel.setInn(Tools.getRandomInRange(1000000000,2147483640));
        personModel.setPostIndex(Tools.getRandomInRange(100000,999999));
        personModel.setCity(testData.getCity());
        personModel.setStreet(testData.getStreet());
        personModel.setHouseNumber(Tools.getRandomInRange(1,99)+"");
        personModel.setFlatNumber(Tools.getRandomInRange(1,1000));
    }

    public PersonModel getPerson(){
        return personModel;
    }

}
